#include <stdio.h>
int main(int argc, char const *argv[]) {
  /*
   Programa que imprime en pantalla el tamaño en bytes}
  */
  printf("El tamaño de un int es: %lu bytes en memoria\n",sizeof(int) );
  printf("El tamaño de un Float es: %lu bytes en memoria\n",sizeof(float) );
  printf("El tamaño de un char es: %lu bytes en memoria\n",sizeof(char) );
  printf("El tamaño de un long int es: %lu bytes en memoria\n",sizeof(long int) );
  return 0;
}
